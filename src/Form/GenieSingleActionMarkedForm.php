<?php

namespace Drupal\genie\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class GenieSingleActionMarkedForm
 *
 * Form for marking actions as completed
 *
 * @package Drupal\genie\Form
 */
class GenieSingleActionMarkedForm extends FormBase
{
    public function getFormId()
    {
        return 'marked-question';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $path = \Drupal::request()->getPathInfo();
        $arg = explode('/', $path);
        $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();

        if ($lang === 'en') {
            $actionNo = $arg[4];
        } else {
            $actionNo = $arg[5];
        }

        $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
        $userId = \Drupal::currentUser()->id();

        $connection = \Drupal::database();
        $actionsCard = $connection->query('SELECT * FROM {genie_actions} WHERE lang=? AND `action`=?', [$lang, $actionNo])->fetchAll();
        $homeTexts = $connection->query('SELECT * FROM {genie_texts} WHERE lang=?', [$lang])->fetchAll();
        $acButtons = unserialize($homeTexts[0]->ac_buttons);
        $currentAction = reset($actionsCard);

        $userProfile = $connection->query('SELECT * FROM {genie_profiles} WHERE users_id=?', [$userId])->fetchAll();
        $actionsCompleted = $userProfile[0]->actions_completed != '' ? unserialize($userProfile[0]->actions_completed) : array();

        if (in_array($currentAction->action, $actionsCompleted)) {
            $form['acNo'] = [
                '#type' => 'hidden',
                '#value' => $currentAction->action
            ];
            $form['marked'] = [
                '#type' => 'hidden',
                '#value' => 0
            ];
            $form['submit'] = [
                '#type' => 'submit',
                '#value' => $acButtons[2],
                '#attributes' => ['class' => ['btn', 'btn-danger']]
            ];
        } else {
            $form['acNo'] = [
                '#type' => 'hidden',
                '#value' => $currentAction->action
            ];
            $form['marked'] = [
                '#type' => 'hidden',
                '#value' => 1
            ];
            $form['submit'] = [
                '#type' => 'submit',
                '#value' => $acButtons[1],
                '#attributes' => ['class' => ['btn', 'btn-success']]
            ];
        }

        return $form;
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $current = \Drupal::currentUser()->id();
        $connection = Database::getConnection();

        $userProfile = $connection->query('SELECT * FROM {genie_profiles} WHERE users_id=?', [$current])->fetchObject();

        $aCompleted = unserialize($userProfile->actions_completed);

        $checked = $form_state->getValue('marked');
        $acNo = $form_state->getValue('acNo');

        if ($checked == 0 && is_array($aCompleted)) {
            $key = array_search($acNo, $aCompleted);
            if ($key != false) {
                unset($aCompleted[$key]);
            }
        } elseif ($checked == 1) {
            $aCompleted[] = $acNo;
        }

        if ($userProfile) {
            $connection->update('genie_profiles')->fields([
                'actions_completed' => serialize($aCompleted)
            ])
                ->condition('users_id', $current, '=')
                ->execute();
        }

    }
}
