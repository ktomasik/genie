<?php

namespace Drupal\genie\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class GenieQuestionForm
 *
 * Form with questions for creating a new strategy
 *
 * @package Drupal\genie\Form
 */
class GenieQuestionForm extends FormBase
{

  public function getFormId()
  {
    return 'question_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state)
  {

    $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();

    $connection = \Drupal::database();
    $query = $connection->query('SELECT * FROM {genie_profile_questions} WHERE lang=?', [$lang]);
    $results = $query->fetchAll();

    $queries = $connection->query('SELECT * FROM {genie_texts} WHERE lang=?', [$lang]);
    $texts = $queries->fetchAll();
    $homeTexts = $texts[0];

    $option = [];

    $form['start'] = [
      '#markup' => '<div id="startScreen" class="screen"><p class="screenHeader text-center">' . $homeTexts->create_strategy1 . '</p>' . $homeTexts->create_strategy2 . '<br/>' . $homeTexts->create_strategy3 . '<div></div >
                            <div class="text-center buttonDiv">
                            <a id="genie-start" class="genie-start btn btn-primary" href="#">' . $homeTexts->create_strategy4 . '</a>
                            </div>
                </div>'
    ];
    for ($i = 1; $i < 12; $i++) {
      $form['qNumber' . $i] = [
        '#markup' => '<div id="qNumberScreen' . $i . '" class="qNumberScreen"><h2 class="text-center">' . $homeTexts->create_strategy5 . ' ' . $i . ' ' . $homeTexts->create_strategy6 . ' 11</h2></div>'
      ];
    }

    foreach ($results as $r => $key) {
      $answers = unserialize($key->answers);
      foreach ($answers as $ans => $val) {
        $option[$r][$r . '-' . $ans] = $val;
      }
      $form['q-' . $r] = [
        '#type'          => 'radios',
        '#title'         => $key->question,
        '#options'       => $option[$r],
        '#default_value' => NULL,
      ];
    }

    $form['summaryScreen'] = [
      '#markup' => '<div id="summaryScreen">
                            <p class="screenHeader text-center">' . $homeTexts->create_strategy8 . '</p>
                            <p>' . $homeTexts->create_strategy9 . '</p>
                            <p>' . $homeTexts->create_strategy10 . '</p>
                        </div>'
    ];

    $form['#attributes'] = array('class' => ['container', 'question-form']);

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type'        => 'submit',
      '#value'       => $homeTexts->create_strategy11,
      '#button_type' => 'primary',
    );

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $points = [0, 0, 0, 0, 0, 0];
    $priorities = [];
    $profileAnswers = [];
    $profilePriorities = [];
    $profileTriggers = [];
    $ict = [];
    $webShopLevel = [];
    $area = '';

    foreach ($form_state->getValues() as $key => $value) {
      switch ($value) {
        case '0-a':
          $points[5] += 1;
          $profileTriggers[] = '0_Yes';
          break;
        case '0-b':
          $profileTriggers[] = '0_No';
          break;
        case '1-a':
          $profileTriggers[] = '1_Yes';
          $profileAnswers[] = '2_a';
          $ict[] = 'Basic';
          break;
        case '1-b':
          $profileTriggers[] = '1_Yes';
          $profileAnswers[] = '2_b';
          $ict[] = 'Intermediate';
          break;
        case '1-c':
          $profileTriggers[] = '1_Yes';
          $profileAnswers[] = '2_c';
          $ict[] = 'Advanced';
          break;
        case '2-a':
          $profileTriggers[] = '2_Yes';
          break;
        case '3-a':
          $points[0] += 1;
          $profileTriggers[] = '3_Yes';
          $profileAnswers[] = '3_a';
          $area = 'Ordering';
          break;
        case '3-b':
          $points[1] += 1;
          $profileTriggers[] = '3_Yes';
          $profileAnswers[] = '3_b';
          $area = 'Payment';
          break;
        case '3-c':
          $points[2] += 1;
          $profileTriggers[] = '3_Yes';
          $profileAnswers[] = '3_c';
          $area = 'Delivery';
          break;
        case '3-d':
          $points[3] += 1;
          $profileTriggers[] = '3_Yes';
          $profileAnswers[] = '3_d';
          $area = 'Communication';
          break;
        case '3-e':
          $points[4] += 1;
          $profileTriggers[] = '3_Yes';
          $profileAnswers[] = '3_e';
          $area = 'Overall Service';
          break;
        case '3-f':
          $points[5] += 1;
          $profileTriggers[] = '3_Yes';
          $profileAnswers[] = '3_f';
          $area = 'Promotion';
          break;
        case '4-a':
          $profileTriggers[] = '4_Yes';
          $profileAnswers[] = '4_a';
          $webShopLevel[] = 'Starting';
          break;
        case '4-b':
          $profileTriggers[] = '4_Yes';
          $profileAnswers[] = '4_b';
          $webShopLevel[] = 'Established';
          break;
        case '4-c':
          $profileTriggers[] = '4_Yes';
          $profileAnswers[] = '4_c';
          $webShopLevel[] = 'Well developed';
          break;
        case '5-a':
          $profileTriggers[] = '5_Yes';
          break;
        case '5-c':
          $profileTriggers[] = '5_No';
          break;
        case '6-a':
          $profileTriggers[] = '6_No';
          break;
        case '6-b':
          $points[0] += 1;
          $profileTriggers[] = '6_Yes';
          break;
        case '7-a':
          $profileTriggers[] = '7_No';
          break;
        case '7-b':
          $points[1] += 1;
          $profileTriggers[] = '7_Yes';
          break;
        case '8-a':
          $profileTriggers[] = '8_No';
          break;
        case '8-b':
          $points[2] += 1;
          $profileTriggers[] = '8_Yes';
          break;
        case '9-a':
          $profileTriggers[] = '9_No';
          break;
        case '9-b':
          $points[3] += 1;
          $profileTriggers[] = '9_Yes';
          break;
        case '10-a':
          $profileTriggers[] = '10_No';
          break;
        case '10-b':
          $points[4] += 1;
          $profileTriggers[] = '10_Yes';
          break;
      }
    }
    $names = ['ORDERING', 'PAYMENT', 'DELIVERY', 'COMMUNICATION', 'OVERALL-SERVICE', 'PROMOTION'];
    for ($i = 0; $i < 6; $i++) {
      $profilePriorities[] = $points[$i] . '_' . $names[$i];
    }
    $priorities['priorities'] = $profilePriorities;
    $priorities['element'] = $profileAnswers;
    $priorities['triggers'] = $profileTriggers;
    $priorities['ict'] = $ict;
    $priorities['webShop'] = $webShopLevel;
    $priorities['area'] = $area;

    $current = \Drupal::currentUser()->id();
    $connection = Database::getConnection();

    $query = $connection->query('SELECT * FROM {genie_profiles} WHERE users_id=?', [$current]);
    $userProfile = $query->fetchObject()->users_id;

    if (!isset($userProfile)) {
      $connection->insert('genie_profiles')->fields([
          'users_id' => $current,
          'profile'  => serialize($priorities)
        ]
      )->execute();
    } else {
      $connection->update('genie_profiles')->fields([
        'profile' => serialize($priorities)
      ])
        ->condition('users_id', $current, '=')
        ->execute();
    }

    $url = Url::fromRoute('genie.report');
    $form_state->setRedirectUrl($url);

  }
}
