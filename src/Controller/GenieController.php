<?php

namespace Drupal\genie\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class GenieController
 *
 * Main page of the Genie
 *
 * @package Drupal\genie\Controller
 */
class GenieController extends ControllerBase
{
  public function content()
  {
    $current = \Drupal::currentUser()->id();
    $connection = \Drupal::database();
    $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();

    $homeTexts = $connection->query('SELECT * FROM {genie_texts} WHERE lang=?', [$lang])->fetchAll();
    $menuText = unserialize($homeTexts[0]->home5);
    $home4_3 = unserialize($homeTexts[0]->home4_3);

    return [
      '#theme'     => 'page-genie-home',
      '#menuText'  => $menuText,
      '#user'      => $current,
      '#homeTexts' => $homeTexts,
      '#home4_3'   => $home4_3,
      '#lang'      => $lang
    ];
  }
}
