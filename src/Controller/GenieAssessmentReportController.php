<?php

namespace Drupal\genie\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class GenieAssessmentReportController
 *
 * Shows the report after filling in the profiling session
 *
 * @package Drupal\genie\Controller
 */
class GenieAssessmentReportController extends ControllerBase
{
    public function content()
    {
        $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
        $connection = \Drupal::database();
        $userId = \Drupal::currentUser()->id();

        $homeTexts = $connection->query('SELECT * FROM {genie_texts} WHERE lang=?', [$lang])->fetchAll();
        $menuText = unserialize($homeTexts[0]->home5);
        $triggersTexts = unserialize($homeTexts[0]->q_triggers);

        $profile = $connection->query('SELECT * FROM {genie_profiles} WHERE users_id=?', [$userId])->fetchAll();
        $profileDecoded = unserialize($profile[0]->profile);
        $pTrigger = $profileDecoded['triggers'];
        $area = $profileDecoded['area'];
        $webShop = $profileDecoded['webShop'];
        $ict = $profileDecoded['ict'];

        $assessmentText = [];
        if (isset($pTrigger)) {
            if (in_array('0_No', $pTrigger, true)) {
                $assessmentText['q1'] = $triggersTexts[0]['no'];
            }
            $assessmentText['q2'][] = $triggersTexts[1][0];
            $assessmentText['q2'][] = $ict[0];
            $assessmentText['q2'][] = $triggersTexts[1][1];
            if (in_array('2_Yes', $pTrigger, true)) {
                $assessmentText['q3'] = $triggersTexts[2]['yes'];
            }
            $assessmentText['q4'][] = $triggersTexts[3][0];
            $assessmentText['q4'][] = $area;
            $assessmentText['q4'][] = $triggersTexts[3][1];
            $assessmentText['q5'][] = $triggersTexts[4][0];
            $assessmentText['q5'][] = $webShop[0];
            $assessmentText['q5'][] = $triggersTexts[4][1];
            if (in_array('5_No', $pTrigger, true)) {
                $assessmentText['q6'] = $triggersTexts[5]['no'];
            } elseif (in_array('5_Yes', $pTrigger, true)) {
                $assessmentText['q6'] = $triggersTexts[5]['yes'];
            }
            if (in_array('6_Yes', $pTrigger, true)) {
                $assessmentText['q7'] = $triggersTexts[6]['no'];
            }
            if (in_array('7_Yes', $pTrigger, true)) {
                $assessmentText['q8'] = $triggersTexts[7]['no'];
            }
            if (in_array('8_Yes', $pTrigger, true)) {
                $assessmentText['q9'] = $triggersTexts[8]['no'];
            }
            if (in_array('9_Yes', $pTrigger, true)) {
                $assessmentText['q10'] = $triggersTexts[9]['no'];
            }
            if (in_array('10_Yes', $pTrigger, true)) {
                $assessmentText['q11'] = $triggersTexts[10]['no'];
            }
        }

        return [
            '#theme' => 'page-assessment-report',
            '#menuText' => $menuText,
            '#assessmentText' => $assessmentText,
            '#profileDecoded' => $profileDecoded,
            '#lang' => $lang
        ];
    }
}
