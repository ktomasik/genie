<?php

namespace Drupal\genie\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class GenieGlossaryController
 *
 * Genie's glossary functionality
 *
 * @package Drupal\genie\Controller
 */
class GenieGlossaryController extends ControllerBase
{

  public function glossary()
  {
    $connection = \Drupal::database();
    $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();

    $homeTexts = $connection->query('SELECT * FROM {genie_texts} WHERE lang=?', [$lang])->fetchAll();
    $menuText = unserialize($homeTexts[0]->home5);

    $letters = [];
    $glossary = [];

    $query = $connection->query('SELECT * FROM {genie_glossary} WHERE lang=? ORDER BY `term`', [$lang]);
    $results = $query->fetchAll();

    foreach ($results as $result) {
      $l = mb_strtoupper(mb_substr($result->term, 0, 1, 'utf-8'), "utf-8");
      $l = is_numeric($l) ? '#' : $l;

      $letters[] = $l;

      if (!isset($glossary[$l])) {
        $glossary[$l] = [];
      }

      $glossary[$l][] = [$result->term, $result->definition];
    }

    $letters = array_unique($letters);

    return [
      '#theme'     => 'page-glossary',
      '#menuText'  => $menuText,
      '#letters'   => $letters,
      '#homeTexts' => $homeTexts,
      '#glossary'  => $glossary,
      '#lang'      => $lang
    ];
  }
}
