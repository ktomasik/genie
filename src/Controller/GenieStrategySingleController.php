<?php

namespace Drupal\genie\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\filter\FilterPluginCollection;

/**
 * Class GenieStrategySingleController
 *
 * Action card display
 *
 * @package Drupal\genie\Controller
 */
class GenieStrategySingleController extends ControllerBase
{
  public function content()
  {
    $path = \Drupal::request()->getPathInfo();
    $arg = explode('/', $path);

    $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $userId = \Drupal::currentUser()->id();

      if($lang === 'en') {
          $actionNo = $arg[4];
      } else {
          $actionNo = $arg[5];
      }
    $connection = \Drupal::database();
    $actionsCard = $connection->query('SELECT * FROM {genie_actions} WHERE lang=? AND `action`=?', [$lang, $actionNo])->fetchAll();
    $homeTexts = $connection->query('SELECT * FROM {genie_texts} WHERE lang=?', [$lang])->fetchAll();
    $acButtons = unserialize($homeTexts[0]->ac_buttons);
    $acSectionNames = unserialize($homeTexts[0]->ac_sections_titles);

    $currentAction = reset($actionsCard);
    $nextAction = $connection->query('SELECT `id`, `action`, `category` FROM `genie_actions` WHERE lang=? AND category=? AND id >? ORDER BY id ASC LIMIT 1', [$lang, $currentAction->category, $currentAction->id])->fetchAll();
    $previousAction = $connection->query('SELECT `id`, `action`, `category` FROM `genie_actions` WHERE lang=? AND `category`=? AND id <? ORDER BY id DESC LIMIT 1', [$lang, $currentAction->category, $currentAction->id])->fetchAll();

    $nextAction = reset($nextAction);
    $previousAction = reset($previousAction);

    $userProfile = $connection->query('SELECT * FROM {genie_profiles} WHERE users_id=?', [$userId])->fetchAll();

    $actionsCompleted = $userProfile[0]->actions_completed != '' ? unserialize($userProfile[0]->actions_completed) : [];

    function parseConnectedActions($s)
    {
      $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $s = preg_replace('/,\s+/', '', $s);
      $pattern = '/\[(.*)\]/U';
      preg_match_all($pattern, $s, $listOfActions);

      $actions = $listOfActions[1];

      $connection = \Drupal::database();

      $result = $connection->query('SELECT * FROM {genie_actions} WHERE action IN (:actions[]) AND lang=:lang', [':actions[]' => $actions, ':lang' => $lang])->fetchAll();

      $actionTitles = [];
      foreach ($result as $r) {
        $actionTitles[$r->action] = $r->title;
      }
      return preg_replace_callback($pattern, function ($match) use ($actionTitles) {
        return '<i class="fas fa-arrow-alt-circle-right menu-icon"></i> <a href="/genie/strategy/action/' . $match[1] . '" class="connectedItem"> ' . $actionTitles[$match[1]] . '</a><br />';
      }, $s);
    }

    /**
     * make links clickable
     */
    // The text processing filters service.
    $manager = \Drupal::service('plugin.manager.filter');
    // Getting filter plugin collection.
    $filter_collection = new FilterPluginCollection($manager, []);
    // Getting the filter_url plugin.
    $filter = $filter_collection->get('filter_url');
    // Setting the filter_url plugin configuration.
    $filter->setConfiguration([
      'settings' => [
        'filter_url_length' => 120,
      ]
    ]);
    $filteredCards = [];
    $connectedActions = '';

        $completed = true;

        foreach ($actionsCard as $actionCard) {
          if ($actionsCompleted != '') {
            $completed = in_array($actionCard->action, $actionsCompleted, true) ? true : false;
          }

          $connectedActions = str_replace('\r\n', '', parseConnectedActions($actionCard->connected));

          $filteredCards['abstract'] = _filter_url(str_replace('\r\n', '<br/>', $actionCard->abstract), $filter);
          $filteredCards['gd_justification'] = _filter_url(str_replace('\r\n', '<br/>', $actionCard->gd_justification), $filter);
          $filteredCards['gd_implementation'] = _filter_url(str_replace('\r\n', '<br/>', $actionCard->gd_implementation), $filter);
          $filteredCards['check_list'] = _filter_url(str_replace('\r\n', '<br/>', $actionCard->check_list), $filter);
          $filteredCards['metrics'] = _filter_url(str_replace('\r\n', '<br/>', $actionCard->metrics), $filter);
          $filteredCards['implications'] = _filter_url(str_replace('\r\n', '<br/>', $actionCard->implications), $filter);
          $filteredCards['tips'] = _filter_url(str_replace('\r\n', '<br/>', $actionCard->tips), $filter);
          $filteredCards['time'] = _filter_url(str_replace('\r\n', '<br/>', $actionCard->time), $filter);
          $filteredCards['estimated'] = _filter_url(str_replace('\r\n', '<br/>', $actionCard->estimated), $filter);
          $filteredCards['links'] = _filter_url(str_replace('\r\n', '<br/>', $actionCard->links), $filter);
          $filteredCards['cross_border'] = _filter_url(str_replace('\r\n', '<br/>', $actionCard->cross_border), $filter);
          $filteredCards['estimated2'] = _filter_url(str_replace('\r\n', '<br/>', $actionCard->estimated2), $filter);
          $filteredCards['impact'] = _filter_url(str_replace('\r\n', '<br/>', $actionCard->impact), $filter);
        }

    $myform = \Drupal::formBuilder()->getForm('Drupal\genie\Form\GenieSingleActionMarkedForm');

    return [
      '#theme'            => 'page-single-action',
      '#actionsCard'      => $actionsCard,
      '#acButtons'        => $acButtons,
      '#acSectionNames'   => $acSectionNames,
      '#currentAction'    => $currentAction,
      '#nextAction'       => $nextAction,
      '#previousAction'   => $previousAction,
      '#actionsCompleted' => $completed,
      '#connectedActions' => $connectedActions,
      '#filteredCards'    => $filteredCards,
      '#form'             => $myform,
      '#lang'             => $lang
    ];
  }


}
