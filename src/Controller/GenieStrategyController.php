<?php

namespace Drupal\genie\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class GenieStrategyController
 *
 * Shows the strategy
 *
 * @package Drupal\genie\Controller
 */
class GenieStrategyController extends ControllerBase
{
  public function content()
  {
    $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();

    $connection = \Drupal::database();

    $userId = \Drupal::currentUser()->id();

    //get actions names
    $actionsCards = $connection->query('SELECT `action`, `title`, `category`, `ict`, `webshop`, `english`, `type` FROM {genie_actions} WHERE lang=?', [$lang])->fetchAll();
    $profile = $connection->query('SELECT * FROM {genie_profiles} WHERE users_id=?', [$userId])->fetchAll();

    $homeTexts = $connection->query('SELECT * FROM {genie_texts} WHERE lang=?', [$lang])->fetchAll();
    $menuText = unserialize($homeTexts[0]->home5);
    $categoryText = unserialize($homeTexts[0]->strategy1);
    $filterText = unserialize($homeTexts[0]->filters);

    $profileDecoded = unserialize($profile[0]->profile);

    $pPriorities = $profileDecoded['priorities'];
    $actionsCompleted = $profile[0]->actions_completed != '' ? unserialize($profile[0]->actions_completed) : [];

    $pICT = $profileDecoded['ict'];

    $prioritiesH = []; // high priorities
    $prioritiesL = []; // low priorities
    $pICTPriorities = '';
    if ($pICT[0] === 'Advanced') {
      $pICTPriorities = ['Basic', 'Intermediate', 'Advanced'];
    } else if ($pICT[0] === 'Intermediate') {
      $pICTPriorities = ['Basic', 'Intermediate'];
    } else if ($pICT[0] === 'Basic') {
      $pICTPriorities = ['Basic'];
    }

    if ($pPriorities) {
      foreach ($pPriorities as $p) {
        $check = explode('_', $p);

        if ($check[0] >= 1)
          $prioritiesH[] = $check[1];
        else
          $prioritiesL[] = $check[1];

        $priorities[] = $p;
      }
    }

    $actions = [];
    $actionsByModules = [];

    $elementsTranslated = [
      'ORDERING'        => $categoryText[0],
      'PAYMENT'         => $categoryText[1],
      'COMMUNICATION'   => $categoryText[2],
      'DELIVERY'        => $categoryText[3],
      'OVERALL-SERVICE' => $categoryText[4],
      'PROMOTION'       => $categoryText[5],
    ];

    if ($actionsCards) {

      foreach ($actionsCards as $r) {
        if (!isset($actions[$r->category])) {
          $actions[$r->category] = array();
        }
        $actions[$r->category][] = $r;
      }

      foreach ($prioritiesH as $pTmp) {
        $pTmp = trim($pTmp);
        $actionsByModules[$pTmp] = $actions[$pTmp];
      }
      foreach ($prioritiesL as $pTmp) {
        $pTmp = trim($pTmp);
        $actionsByModules[$pTmp] = $actions[$pTmp];
      }

    }

    return [
      '#theme'              => 'page-strategy',
      '#menuText'           => $menuText,
      '#actionsByModules'   => $actionsByModules,
      '#elementsTranslated' => $elementsTranslated,
      '#actionsCompleted'   => $actionsCompleted,
      '#pICTPriorities'     => $pICTPriorities,
      '#filterText'         => $filterText,
      '#lang'               => $lang
    ];
  }
}
