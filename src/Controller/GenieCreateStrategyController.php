<?php

namespace Drupal\genie\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class GenieCreateStrategyController
 *
 * Controller responsible for handling the creation of a new strategy
 *
 * @package Drupal\genie\Controller
 */
class GenieCreateStrategyController extends ControllerBase
{
  public function content()
  {
    $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $connection = \Drupal::database();

    $homeTexts = $connection->query('SELECT * FROM {genie_texts} WHERE lang=?', [$lang])->fetchAll();
    $menuText = unserialize($homeTexts[0]->home5);

    $myform = \Drupal::formBuilder()->getForm('Drupal\genie\Form\GenieQuestionForm');

    return [
      '#theme'    => 'page-create-strategy',
      '#menuText' => $menuText,
      '#form'     => $myform,
      '#lang'     => $lang
    ];
  }
}
