/**
 * @file
 */
(function ($, Drupal, drupalSettings) {
    'use strict';
    Drupal.behaviors.jsGenie = {

        attach: function (context, settings) {

            //remove <br/> between <li> tags
            $('ul > br').remove();
            $('ol > br').remove();
            /**
             * remove news from genie
             */
            $('section.featured-bottom, section.featured-bottom-highlighted, section.newsletter').hide();

            $('img.actionImage').parent().addClass('text-center');

            $('#genie-start').once('jsGenie').on('click', function () {
                $('#startScreen').hide();
                $('#edit-q-0--wrapper').show(400);
                $('#qNumberScreen1').show();
            });

            var summaryScreen = $('#question-form #summaryScreen');
            var showResultsBtn = $('#question-form div#edit-actions');

            summaryScreen.append(showResultsBtn);
            showResultsBtn.addClass('text-center');

            var url = window.location.pathname.split('/');
            var lang = url[1];
            var nonFilled = '';
            var buttonNext = '';
            var buttonFinish = '';

            if (lang === 'pt-pt') {
                nonFilled = 'A pergunta não foi respondida!';
                buttonNext = 'Próximo';
                buttonFinish = 'Terminar';
            } else if (lang === 'de') {
                nonFilled = 'Frage wurde nicht beantwortet!';
                buttonNext = 'Nächster';
                buttonFinish = 'Fertig';
            } else if (lang === 'el') {
                nonFilled = 'Η ερώτηση δεν απαντήθηκε!';
                buttonNext = 'Επόμενο';
                buttonFinish = 'Φινίρισμα';
            } else if (lang === 'pl') {
                nonFilled = 'Nie udzielono odpowiedzi na pytanie!';
                buttonNext = 'Następne';
                buttonFinish = 'Zakończ';
            } else if (lang === 'ro') {
                nonFilled = 'Întrebare nu a fost răspuns!';
                buttonNext = 'Următor ';
                buttonFinish = 'Termina';
            } else {
                nonFilled = 'Question has not been answered!';
                buttonNext = 'Next';
                buttonFinish = 'Finish';
            }

            var formScreens = $('.fieldgroup');

            $(formScreens, context).once('jsGenie').each(function (index) {
                var screenId = $(this).attr('id');
                if (screenId === 'edit-q-0--wrapper' || screenId === 'edit-q-1--wrapper' || screenId === 'edit-q-2--wrapper' || screenId === 'edit-q-3--wrapper' || screenId === 'edit-q-4--wrapper' || screenId === 'edit-q-5--wrapper' || screenId === 'edit-q-6--wrapper' || screenId === 'edit-q-7--wrapper' || screenId === 'edit-q-8--wrapper' || screenId === 'edit-q-9--wrapper') {
                    $(this).append('<div class="btnNext current-' + index + ' col-md-12 text-right"><a id="btnNext-' + index + '" data-question="' + index + '" class="genie_next btn btn-primary button-form" href="#">'+ buttonNext +'</a>' +
                        '<br><span class="notfilled" >'+ nonFilled +'</span>' +
                        '</div>');
                } else if (screenId === 'edit-q-10--wrapper') {
                    $(this).append('<div class="btnNext current-' + index + ' col-md-12 text-right"><a id="btnNext-' + index + '" data-question="' + index + '" class="genie_next genieShowSummary btn btn-primary button-form" href="#">'+buttonFinish+'</a>' +
                        '<br><span class="notfilled" >'+ nonFilled +'</span>' +
                        '</div>');
                } else {
                    $(this).append($('#edit-actions'));
                    $('#edit-actions', context).once('jsGenie').append('<span class="notfilled" >'+ nonFilled +'</span>');
                    $('#edit-actions button', context).once('jsGenie').addClass('button-form');
                    $('#edit-actions', context).once('jsGenie').addClass('col-md-2');
                }


            });

            var btnNext = $('.genie_next');

            $(btnNext, context).once('jsGenie').each(function () {
                var current = parseInt($(this).attr('data-question'));

                $(this).on('click', function (e) {
                    e.preventDefault();
                    if (!$('#edit-q-' + current + ' input').is(':checked')) {
                        $('.current-' + current + ' .notfilled').show(400);
                        return;
                    }
                    $('#edit-q-' + current + '--wrapper').hide();
                    $('#edit-q-' + (current + 1) + '--wrapper').show(400);
                    $('#qNumberScreen' + (current + 1)).hide();
                    $('#qNumberScreen' + (current + 2)).show(400);
                });
            });

            var btnShowSummary = $('.genieShowSummary');

            $(btnShowSummary).on('click', function (e) {
                e.preventDefault();

                $('#edit-q-10--wrapper').hide();
                $('#summaryScreen').show(400);

            });

            //action card display ac sections
            var showSingle = $('.showSingle');

            $(showSingle, context).once('jsGenie').each(function () {
                var current = parseInt($(this).attr('data-value'));

                $(this).on('click', function () {
                    $(this).addClass("bg-darkOrange").removeClass('bg-orange');
                    $('.showSingle').not(this).removeClass("bg-darkOrange").addClass('bg-orange');
                    $('.targetDiv').hide();
                    var contentTable = $('#table' + current);
                    contentTable.show("slow");
                })
            });

            //remove default class in markQuestion form
            let qMarkAction = $('#marked-question input.form-submit');
            qMarkAction.removeClass('button');

            //strategy page
            function refreshFilters() {
                $('#genieActions div.actionItem').css({'backgroundColor': '', 'padding': ''});
                $('#genieActions div.actionItem a').css({'color': '', 'font-weight': ''});
                $('#genieActions div.actionItem i').css('color', '#ff6600');
                $('#filterStatus').attr('style', '');

                let filterChain = '';
                let filterChain2 = '';
                let filterChain3 = '';
                let filterCount = 0;
                let filterTotal = $('#genieActions div.actionItem').length;

                $('#genieFilters a.genieFilter').each(function () {

                    if ($(this).attr('data-active') === '1') {
                        filterChain += '[' + $(this).attr('data-type') + '="' + $(this).attr('data-value') + '"]';
                    }

                });
                if (filterChain) {
                    $('#genieActions div.actionItem' + filterChain).css({'backgroundColor': '#ff66008c', 'padding': '1px 5px'});
                    $('#genieActions div.actionItem' + filterChain + ' a').css({'color': '#fff', 'font-weight': 'bold'});

                    if (filterChain2) {
                        $('#genieActions div.actionItem' + filterChain2).css({'backgroundColor': '#ff66008c', 'padding': '1px 5px'});
                        $('#genieActions div.actionItem' + filterChain2 + ' a').css({'color': '#fff', 'font-weight': 'bold'});
                    }
                    if (filterChain3) {
                        $('#genieActions div.actionItem' + filterChain3).css({'backgroundColor': '#ff66008c', 'padding': '1px 5px'});
                        $('#genieActions div.actionItem' + filterChain3 + ' a').css({'color': '#fff', 'font-weight': 'bold'});
                    }

                    filterCount = $('#genieActions div.actionItem' + filterChain).length;

                    if (filterChain2)
                        filterCount += $('#genieActions div.actionItem' + filterChain2).length;

                    if (filterChain3)
                        filterCount += $('#genieActions div.actionItem' + filterChain3).length;

                    $('#filterStatus').html('Highlighting ' + filterCount + ' out of ' + filterTotal + ' total actions based on your selection.');
                    if (filterCount === 0) {
                        $('#filterStatus').css('color', 'red').css('fontWeight', 'bold');
                    }
                }
                else {
                    $('#filterStatus').html('inactive');
                }
            }

            $('.filterReset').parent().find('a').attr('data-active', 0).css('color', '#4b4b4d');

            $('a.genieFilter').click(function (event) {
                event.preventDefault();
                let that = $(this);
                $(this).parent().parent().find('a.genieFilter').each(function () {
                    if (!that.is(this)) {
                        $(this).attr('data-active', 0);
                        $(this).parent().find('i').css('color', '#4b4b4d').removeClass('fa fa-check');
                        $(this).css('color', '#4b4b4d');
                    }
                });

                if ($(this).attr('data-active') === 1) {
                    $(this).attr('data-active', 0);
                    $(this).parent().find('i').css('color', '#4b4b4d').removeClass('fa fa-check');
                    $(this).css('color', '#4b4b4d');
                }
                else {
                    $(this).attr('data-active', 1);
                    $(this).parent().find('i').css('color', '#ff6600').addClass('fa fa-check');
                    $(this).css('color', '#ff6600');
                }

                $(this).blur();

                refreshFilters();
            });

            $('#filtersBtn').click(function (event) {
                event.preventDefault();
                $('#genieFilters').toggle(200);
            });

            $('.filterReset').click(function (event) {
                event.preventDefault();
                $(this).parent().find('a').attr('data-active', 0).css('color', '#4b4b4d');
                $(this).parent().find('i').css('color', '#4b4b4d').removeClass('fa fa-check');
                refreshFilters();
            });

        }
    }
})(jQuery, Drupal, drupalSettings);
