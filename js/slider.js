(function ($, Drupal) {
    'use strict';
    Drupal.behaviors.jsSlider = {

        attach: function () {
            $(document).ready(function(){
                // options
                let speed = 2000; //transition speed - fade
                let autoswitch = true; //auto slider options

                // add first initial active class
                $(".slide").first().addClass("active");

                // hide all slides
                $(".slide").hide();

                // show only active class slide
                $('.active').fadeIn(speed);

                // Next Event Handler
                $('#next').once('jsSlider').on('click', nextSlide);// call function nextSlide

                // Prev Event Handler
                $('#prev').once('jsSlider').on('click', prevSlide);// call function prevSlide

                // Auto Slider Handler
                if(autoswitch === true){
                    setInterval(nextSlide,4000);// call function and value 4000
                }

                // Switch to next slide
                function nextSlide(){
                    $('.active').removeClass('active').addClass('oldActive');
                    if($('.oldActive').is(':last-child')){
                        $('.slide').first().addClass('active');
                    } else {
                        $('.oldActive').next().addClass('active');
                    }
                    $('.oldActive').removeClass('oldActive');
                    $('.slide').fadeOut(speed);
                    $('.active').fadeIn(speed);
                }

                // Switch to prev slide
                function prevSlide(){
                    $('.active').removeClass('active').addClass('oldActive');
                    if($('.oldActive').is(':first-child')){
                        $('.slide').last().addClass('active');
                    } else {
                        $('.oldActive').prev().addClass('active');
                    }
                    $('.oldActive').removeClass('oldActive');
                    $('.slide').fadeOut(speed);
                    $('.active').fadeIn(speed);
                }

                $('#slider').hover(
                    function () {
                        $('#next, #prev').show(300);
                    }, function() {
                        $('#next, #prev').hide(300);
                    }
                );
            });

        }
    }
})(jQuery, Drupal);